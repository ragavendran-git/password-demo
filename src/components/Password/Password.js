import React, { useState } from "react";

import FormInput from "../UI/Form/FormInput/FormInput";
import usePasswordValidation from "../../utils/usePasswordValidation";

const Password = () => {
  const [isFormValid, setIsFormValid] = useState(false);
  const { values, errors, handleChange, handleSubmit } = usePasswordValidation({
    onSubmit(values, errors) {
      setIsFormValid(false);
      if (!errors.length) {
        //logic when form is valid
        setIsFormValid(true);
      }
    }
  });

  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        <FormInput
          label="Password"
          name="password"
          type="password"
          value={values.password}
          className={errors.length ? "is-invalid" : ""}
          onChange={handleChange}
        />
        <FormInput
          label="Confirm password"
          name="confirmPassword"
          type="password"
          value={values.confirmPassword}
          className={errors.length ? "is-invalid" : ""}
          onChange={handleChange}
        />

        {errors.map((error, index) => (
          <div className="text-danger" key={index}>
            {error}
          </div>
        ))}

        <div className="mt-3">
          <input type="submit" className="btn btn-primary" value="Submit" />
          {isFormValid && <span className="text-success ml-3">Success</span>}
        </div>
      </form>
    </div>
  );
};

export default Password;
