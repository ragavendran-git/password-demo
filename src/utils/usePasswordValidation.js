import { useState } from "react";

const usePasswordValidation = ({
  fieldNames = { password: "password", confirmPassword: "confirmPassword" },
  onSubmit
}) => {
  const [values, setValues] = useState({
    [fieldNames.password]: "",
    [fieldNames.confirmPassword]: ""
  });
  const [errors, setErrors] = useState([]);

  const validatePassword = (password) => {
    const arr = [];
    if (password.length < 6) {
      arr.push("Password length should have atleast 6 characters");
    }
    if (!/\d/.test(password)) {
      arr.push("Password should have atleast 1 number");
    }
    if (!/[A-Z]/.test(password)) {
      arr.push("Password should have atleast 1 uppercase character");
    }
    if (!/[a-z]/.test(password)) {
      arr.push("Password should have atleast 1 lowercase character");
    }
    if (!/[!@#$%^&*()_\-+={[}\]\\|:;"'<,>.]/.test(password)) {
      arr.push("Password should have atleast 1 special character");
    }
    return arr;
  };

  const handleChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    setValues({
      ...values,
      [name]: value
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const errors = [];
    errors.push(...validatePassword(values[fieldNames.password]));
    if (values[fieldNames.password] !== values[fieldNames.confirmPassword]) {
      errors.push("Passwords didn't match");
    }
    setErrors([...errors]);
    onSubmit(values, errors);
  };
  return { values, errors, handleChange, handleSubmit };
};

export default usePasswordValidation;
