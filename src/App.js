import Password from "./components/Password/Password";

import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "./styles.css";

export default function App() {
  return (
    <div className="App">
      <Password />
    </div>
  );
}
